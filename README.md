# FORKIO – an upskill project

### Purpose

The project is a clone of broadly available bootstrap-based example. Due to .psd files being available, webpage was written to learn frontend basics including HTML, CSS, SCSS, responsiveness, Gulp and a bit of JS. Another achievement was to follow .psd design as closely as possible.
 
### Dev guide

To run the project you need to have `npm` installed. With that requirement met go to `./` of the project and run:
1. `npm install` – to fetch dependencies
2. `gulp` – to run the environment (server, live reload)

#### Backend
To run the project you need to have java 8 installed.

1. Just cd into it's root folder and `./gradlew bootrun` if using *NIX OS, otherwise `gradlew bootrun`.
